FROM python:3.7
MAINTAINER Zoltan Sandor <zoltan@sandor.xyz>

RUN set -ex && pip install transifex-client
RUN apt-get update && apt-get install -y gettext